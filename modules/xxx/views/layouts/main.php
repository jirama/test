<?php

/* @var $content */

?>
<!doctype html>
<html lang="<?= Yii::$app->session->get("lang") ?>">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
	<title>E-GS</title>
	<meta name="description" content=""/>
	<meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0"/>
	<link
		href="//fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=latin,latin-ext,cyrillic,cyrillic-ext"
		rel="stylesheet"/>
	<link href="<?= Yii::getAlias("@web/") ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="<?= Yii::getAlias("@web/") ?>css/essentials.css" rel="stylesheet"/>
	<link href="<?= Yii::getAlias("@web/") ?>css/layout.css" rel="stylesheet"/>
	<link href="<?= Yii::getAlias("@web/") ?>css/color_scheme/green.css" rel="stylesheet"/>
	<link href="<?= Yii::getAlias("@web/") ?>css/layout-datatables.css" rel="stylesheet"/>
	<link href="<?= Yii::getAlias("@web/") ?>../modules/egs/css/georgie.css" rel="stylesheet"/>
	<link href="<?= Yii::getAlias("@web/") ?>../modules/egs/css/daterangepicker.css" rel="stylesheet"/>
</head>
<body>
<script type="text/javascript">const plugin_path = 'plugins/';</script>
<script src="<?= Yii::getAlias("@web/") ?>plugins/jquery/jquery-2.1.4.min.js"></script>
<div id="wrapper" class="clearfix">
	<header id="header">
		<button id="mobileMenuBtn"></button>
		<span class="logo pull-left">
			<img src="<?= Yii::getAlias('@web/') ?>images/logo_light.png" alt="admin panel" height="35"/>
		</span>
	</header>
	<aside id="aside">
		<nav id="sideNav">
			<ul class="nav nav-list">
				<li>
					<a class="dashboard" href="<?= Yii::getAlias("@web/xxx") ?>">
						<i class="main-icon fa fa-home"></i>
						<span>Home</span>
					</a>
				</li>
			</ul>
		</nav>
		<span id="asidebg"></span>
	</aside>
	<section id="middle">
		<div id="content" class="padding-20">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-transparent">
					<div class="header">
						<strong>TEST</strong>
					</div>
				</div>
				<div class="panel-body">
					<?= $content ?>
				</div>
			</div>
		</div>
	</section>
</body>
<script src="<?= Yii::getAlias("@web/") ?>js/app.js"></script>
<script src="<?= Yii::getAlias("@web/") ?>js/list.js"></script>
<script src="<?= Yii::getAlias("@web/") ?>../modules/egs/js/app.min.js?id=<?= uniqid() ?>"></script>
<script src="<?= Yii::getAlias("@web/") ?>../modules/egs/js/georgie.js"></script>
</html>
