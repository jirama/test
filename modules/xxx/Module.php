<?php

namespace app\modules\xxx;

/**
 * xxx module definition class
 */
class Module extends \yii\base\Module
{
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\xxx\controllers';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		\Yii::$app->request->enableCsrfValidation = false;

		\Yii::setAlias('@here', __DIR__);

		$this->layout = '@here/views/layouts/main';
	}
}
